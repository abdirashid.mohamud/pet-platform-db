# P.E.T Platform Database

This database is designed to support the P.E.T. platform, a new social network and webshop targeting pet owners. The platform is scheduled to launch in 2024 and will allow users to:

- Create groups
- Post to those groups
- Send private messages to each other
- Send orders for products
- Track the inventory and status of those orders

## There are 9 tables in this database:

Users table: This table stores information about the users who sign up to the platform. It has columns for UserID (an auto-incrementing unique identifier for each user), Email, Password, Address, and RoleID. The RoleID column is a foreign key that references the Roles table and indicates the role of the user (i.e., customer, product owner, scrum master, or developer).

Roles table: This table stores the different roles that a user can have. It has columns for RoleID (an auto-incrementing unique identifier for each role) and RoleName (a descriptive name for each role).

UserGroups table: This table stores information about the groups that users create or join. It has columns for UserGroupID (an auto-incrementing unique identifier for each group), GroupID (a foreign key that references the Groups table), and UserID (a foreign key that references the Users table).

Groups table: This table stores information about the groups that users create or join. It has columns for GroupID (an auto-incrementing unique identifier for each group) and GroupName (a descriptive name for each group).

GroupPosts table: This table stores the posts that users make to the groups they belong to. It has columns for PostID (an auto-incrementing unique identifier for each post), GroupID (a foreign key that references the Groups table), UserID (a foreign key that references the Users table), PostText (the content of the post), and PostDate (the date and time the post was made).

PrivateMessages table: This table stores information about the private messages that users send to each other. It has columns for MessageID (an auto-incrementing unique identifier for each message), SenderID (a foreign key that references the Users table), RecipientID (a foreign key that references the Users table), MessageText (the content of the message), and MessageDate (the date and time the message was sent).

Orders table: This table stores information about the orders that users place. It has columns for OrderID (an auto-incrementing unique identifier for each order), UserID (a foreign key that references the Users table), OrderDate (the date and time the order was placed), and OrderStatus (the status of the order, which can be one of: submitted, packing, ready to ship, shipped, or cancelled).

Products table: This table stores information about the products that users can order. It has columns for ProductID (an auto-incrementing unique identifier for each product), ProductName (the name of the product), and InventoryStatus (the inventory status of the product, which can be either in stock or out of stock).

OrderLines table: This table stores information about the products that are ordered within an order. It has columns for OrderLineID (an auto-incrementing unique identifier for each order line), OrderID (a foreign key that references the Orders table), ProductID (a foreign key that references the Products table), Quantity (the quantity of the product ordered), and Price (the price of the product ordered).

Overall, this database schema allows users to sign up to the platform, create or join groups, make posts to those groups, send private messages to each other, place orders for products, and track the inventory and status of those orders. The design is normalized and includes appropriate foreign key relationships to ensure data integrity and consistency
